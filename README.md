# G1Billet

Vous voulez des Ǧ1 sur papier ?

Imprimez vos billets !

Attention, faire des faux ou les laisser circuler est puni par la loi,
et par la perte de confiance de ceux avec qui vous commercez.

## Principe

- Avant de créer un G1Billet, vous transférez le montant correspondant sur un compte porte feuille automatiquement créé pour ce billet.
- Une fois imprimé, vous pouvez vérifier que ce montant est toujours sur le compte via le QR-Code de vérificaiton au recto du billet.
- Pour Encaissez numériquement le billet, vous devrez découper les fragements de QR-Code au verso (et donc détruire le G1billet)
pour reconstituer le QR-Code d'encaissement et l'utiliser pour verser le solde du compte porte feuille du billet vers votre compte.

### Exemple

![recto](recto.png)

![verso](verso.png)

## Fiabilité
Un billet que vous pouvez imprimer vous même, c'est facile à falcifier.
L'alternative est de concentrer le pouvoir d'émission de billet chez un tier de confiance qui imprimera de couteux billet infalcifiable (ou difficile à falcifier), et de vérifier que ce tier de confiance n'abuse pas de sa confiance pour faire n'importe quoi.

Du coup, sans tier de confiance, comment limiter la casse ?
- En rappellant que tromper (avec des faux billets) est puni par la loi (même tromper par négligeance/inadvertance)
- En facilitant la vérification de la validité du billet à un instant T grace au QR-code de vérification
- En rendant les faux (par duplication d'un billet) plus voyant grace au identicon de chaque billet (visage stylisé unique pour chaque billet)
- Grace aux recommandations de vérificiation imprimé sur le billet.

Le principe d'avoir à découper le billet pour l'encaisser n'empèche pas un utilisateur malveillant de frauder,
mais écarte la diffusion d'un G1Billet après encaissement numérique par innadvertence ou mécompréhension du fonctionnement.

### Qu'est ce qu'un faux G1Billet ?

- Un billet dupliqué (photocopié ou autre procédé)
- Un billet vide (dont le QR-Code de vérification pointe vers un compte vide
que ce soit parce qu'il n'a pas été crédité au départ
ou parcequ'il a été encaissé sans être détruit)

### Qu'est ce qu'un vrai G1Billet ?

Un billet unique imprimé recto-verso, associé à un portefeuille unique crédité du montant indiqué sur le G1billet.

## Je veux imprimer des G1Billets maintenant !

Patience, pour l'instant, G1Billet n'est qu'une idée.
Le logiciel viendra !, en attendant, vous pouvez toujours aller jetter un oeil du coté de https://git.duniter.org/tools/paperwallet

## Roadmap

- [ ] [web+g1](https://forum.duniter.org/t/web-g1-clef-publique-ou-autre/4594) syntaxe, page d'enregistrement vers un service
- [ ] lib d'interaction avec la blockchain
  - [ ] consulter le solde d'un compte
  - [ ] transférer d'un compte vers 1 à n comptes
- [ ] [face-identicon](https://1000i100.frama.io/face-identicon/) générateur de visage déterministe ou [jdenticon](https://jdenticon.com/) ou [identicon](http://identicon.net/)
- [ ] page de scan de QR-code pour déclancher les vérification [barcode-scanner](https://github.com/code-kotis/barcode-scanner) ou [pwa-qr-code-scanner](https://github.com/Minishlink/pwa-qr-code-scanner) (ou approche [react](https://github.com/moaazsidat/react-native-qrcode-scanner)-[native](https://github.com/MarnoDev/AC-QRCode-RN)
- [ ] page de vérification du solde d'un compte `web+g1://is:amount/on:pubkey`
  - [ ] afficher si valide
  - [ ] afficher si invalide + motif (vide, montant invalide (inferieur ou superieur) + instruction de destruction sinon complicité)
  - [ ] afficher "encaissement numérique recommandé" (selon heuristique à la discression du point de vérification)
      - [ ] superieur à l'écart type du nombre de vérification par hashIP différente sur les 7 derniers jours
      - [ ] kilométrage entre les lieux de vérification sur les 7 derniers jours superieurs à l'écart type
      - [ ] compte éméteur du G1billet commun avec un compte à l'origin de G1Billet devenu invalides (idem pour les comptes ancètres du compte éméteur)
  - [ ] si le billet proviens directement d'un compte membre, affiche "imprimé par {nom cesium} ou {Uid} + {pubKey}"
  - [ ] si un lieu est indiqué dans la transaction de création, affiche "imprimé à {lieu} + distance au lieu actuel + afficher sur une carte"
  - [ ] collect de statistique de vérification
      - [ ] nombre de scan valide
      - [ ] nombre de scan invalide
      - [ ] pubkey
      - [ ] hashIP
      - [ ] periode (date début, date fin)
      - [ ] lieu du scan (gps) (basé sur ce que le navigateur répond ou a défaut sur du géoip)
      - [ ] portail/site fournisseur de la donnée (pour fédérer les stats entre plusieurs protails)
  - [ ] affichage des statistiques de vérification
      - [ ] % de vérif valide
      - [ ] % de vérif unique valide (pubkey-hashIP unique)
      - [ ] filtre temporel (depuis toujours, de tel date à tel date, dernier mois, semaine, jour...)
      - [ ] filtre géographique (lieu + rayon)
      - [ ] filtre par portail
      - [ ] faux G1billet répendus -> image recto en particulier avec leur face-identicon
- [ ] page de préparation à la création de G1Billets (sélection des billets à créer, du compte à débiter, des dons éventuels, et pourquoi pas du lieu d'émission(stocké dans la transaction))
- [ ] approvisonnement des G1Billets en fabrication
- [ ] génération des images de G1Billet
  - [ ] intégration du QRCode de vérification de validité
  - [ ] intégration du face-identicon
  - [ ] intégration du QRCode d'encaissement fragmenté
  - [ ] intégration du montant
  - [ ] intégration des recommandations d'usage recto et verso
- [ ] génération du pdf avec les G1Billets à imprimer agencé sur feuille A4 recto verso
- [ ] page d'encaissement `web+g1://collect:privatekey`
  - [ ] selection du compte cible
  - [ ] si commission à l'encaissement (déconseillé) information de l'utilisateur du montant de la commission
  - [ ] encaissement mono-cible (ou multi-cible en cas de commission)
  - [ ] compte rendu d'encaissement.
